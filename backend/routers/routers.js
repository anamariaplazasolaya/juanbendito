const express = require('express');
const router = express.Router();
const controladorProductos = require('./router_productos');
const controladorCategorias = require('./router_categorias');

router.use("/productos",controladorProductos);
router.use("/categorias",controladorCategorias);

module.exports = router



