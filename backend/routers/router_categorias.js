const express = require('express');
const router = express.Router();

//CONFIGURACION DE LA RUTA CON EL METODO <<SUGERIDO>> POR EL PROFESOR
const controladorCategorias = require('../controllers/controller_categorias');
router.get("/listar",controladorCategorias);
router.get("/cargar/:id",controladorCategorias);
router.post("/agregar",controladorCategorias);
router.post("/editar/:id",controladorCategorias);
router.delete("/borrar/:id",controladorCategorias);

/*
//CONFIGURACION DE LA RUTA CON EL METODO SUGERIDO EN LA GUIA
const controladorProductosGuia = require('../controllers/controller_productos_guia');
router.get("/listarguia/:id?",controladorProductosGuia.productosListar);
router.post("/agregarguia/:id?",controladorProductosGuia.productosAgregar);
router.post("/editarguia/:id?",controladorProductosGuia.productosEditar);
router.delete("/borrarguia/:id?",controladorProductosGuia.productosBorrar);

//CONFIGURACION DE LA RUTA CON EL METODO LEONARDO
const controladorProductosLeonardo = require('../controllers/controller_productos_leonardo');
router.get("/listarleonardo/:id?",controladorProductosLeonardo.productosListar);
router.get("/cargarleonardo/:id?",controladorProductosLeonardo.productosCargar);
router.post("/agregarleonardo/:id?",controladorProductosLeonardo.productosAgregar);
router.post("/editarleonardo/:id?",controladorProductosLeonardo.productosEditar);
router.delete("/borrarleonardo/:id?",controladorProductosLeonardo.productosBorrar);
*/

//CONFIGURACIÓN DE LAS RUTAS PARA REALIZAR CONSULTAS
const controladorProductosCategorias = require('../controllers/controller_productoscategorias');
router.get("/productoscategorias",controladorProductosCategorias);
router.get("/productoscategorias/:id?",controladorProductosCategorias);

module.exports = router
