const express = require('express');
const router = express.Router();
const modeloCategoria = require('../models/model_categorias');
const modeloProducto = require('../models/model_productos');
const miconexion = require('../conexion');

//CONSULTA USANDO EL METODO AGGREGATE DE MONGODB
router.get('/productoscategorias', (req, res) => {
    modeloCategoria.aggregate([{
        $lookup: {
            localField: "id",
            from: "productos",
            foreignField: "id_categoria",
            as: "productos_categoria",
        },
    },
    {
        $unwind: "$productos_categoria",
    }
    ])
    .then((result)=>{res.send(result); console.log(result)})
    .catch((error)=>{res.send(error); console.log(error)});
})

//Cargar un producto http://localhost:5000/api/productos/cargar/1
router.get('/productoscategorias/:id', (req, res) => {
//CONSULTA CON METODOS JAVASCRIPT Y MONGODB FILTRANDO POR ALGUN CAMPO DE LA COLECCION
var dataCategorias = [];
//modeloCategoria.find({ nombre: "Comestibles" }).then(data => {
modeloCategoria.find({id:req.params.id}).then(data => {
        console.log("Categoria:")
        console.log(data);
        data.map((d, k) => {dataCategorias.push(d.id);})
        modeloProducto.find({ id_categoria: { $in: dataCategorias } })
            .then(data => {
                console.log("Productos de la categoria:")
                console.log(data);
                res.send(data);
            })
            .catch(error => {
                console.log(error);
                res.send(error);
            })
    })
    .catch(error => {
        console.log(error);
        res.send(error);
    })
})

module.exports = router;
