const express = require('express');
const router = express.Router();
const modeloCategoria = require('../models/model_categorias');

//METODO <<SUGERIDO>> POR EL PROFESOR
//Cargar un producto http://localhost:5000/api/categorias/cargar/
router.get('/listar', (req, res) => {
    modeloCategoria.find({}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

//http://localhost:5000/api/categorias/agregar/
router.post('/agregar', (req, res) => {
    
    const nuevaCategoria = new modeloCategoria({
        id: req.body.id,
        nombre: req.body.nombre,
        activo: req.body.activo
    })
    //nuevoProducto._id = new mongoose.Types.ObjectId();
    //nuevoProducto.set('versionKey', false);
    nuevaCategoria.save(function(err)
    {
        if(!err)
        {
            res.send('La categoria fue agregada exitosamente!!!')
        }
        else
        {
            res.send(err.stack)
        }
    })
})

//Cargar un producto http://localhost:5000/api/categorias/cargar/1
router.get('/cargar/:id', (req, res) => {
    modeloCategoria.find({id:req.params.id}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

//http://localhost:5000/api/categorias/editar/2
router.post('/editar/:id', (req, res) => {
    modeloCategoria.findOneAndUpdate(
        {id:req.params.id}
        ,{
            nombre: req.body.nombre,
            activo: req.body.activo
        },
        (err) =>
        {
            if(!err)
            {
                res.send("La categoria se actualizó exitosamente!!!")
            }
            else
            {
                res.send(err)
            }
        })
})

//http://localhost:5000/api/categorias/borrar/3
router.delete('/borrar/:id', (req, res) => {
    modeloCategoria.findOneAndDelete(
        {id:req.params.id},
        (err) =>
        {
            if(!err)
            {
                res.send("La categoria se elimino exitosamente!!!")
            }
            else
            {
                res.send(err)
            }
        })
})

module.exports = router